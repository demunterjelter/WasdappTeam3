/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.wasdapp.repositories;

import com.realdolmen.wasdapp.exceptions.NoQueryPossibleException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author demun
 */
public abstract class AbstractRepository<C,T> {
    public static String LOGIN;
    public static String PASSWORD;
    public static String DRIVER = "com.mysql.jdbc.Driver";
    private String tableName;
    private String idName;
    private String url;
    
     public AbstractRepository(String tableName, String idName) {
         Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("./settings.cfg"));
            url = prop.getProperty("URL");
            LOGIN = prop.getProperty("LOGIN");
            PASSWORD = prop.getProperty("PASSWORD");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AbstractRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AbstractRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.tableName = tableName;
        this.idName = idName;
    }
     
      protected AbstractRepository(String tableName, String idName, String url) {
                 Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("./settings.cfg"));
            LOGIN = prop.getProperty("LOGIN");
            PASSWORD = prop.getProperty("PASSWORD");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AbstractRepository.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AbstractRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
          this.idName = idName;
        this.tableName = tableName;
        this.url = url;
    }
      public Connection createConnection() throws SQLException {
          System.out.println("Setting up connection...");
        return DriverManager.getConnection(url, LOGIN, PASSWORD);
    }
      
    public List<C> findAll() throws NoQueryPossibleException {
        List<C> listToFill = null;
        try (Connection connection = createConnection()) {
            System.out.println("connection succes");
            PreparedStatement pstatement = connection.prepareStatement("SELECT * FROM " + tableName);
            ResultSet resultSet = pstatement.executeQuery();
            listToFill = new ArrayList<>();
            while (resultSet.next()) {
                listToFill.add(createObject(resultSet));
            }
        } catch (Exception e) {
            throw new NoQueryPossibleException("Find all " + tableName + " can not be excecuted");
        }
        return listToFill;
    }
     public abstract C createObject(ResultSet resultSet);
     
     public abstract String getColumnString();

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
}
