/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.realdolmen.wasdapp;

import java.io.FileWriter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;



/**
 *
 * @author LNSBG31
 */
public class JsonWriter {
    
    public static void main(String[]args)
    {
        JSONObject jsonObject = new JSONObject();
        
          //JSON object and values
          jsonObject.put("naam:" , "Frank");
          jsonObject.put("locatie", "GENT");
          
          JSONArray jsonArray = new JSONArray();
          jsonArray.add("manier");
          jsonArray.add("zoek");
          jsonArray.add("data");
          
          jsonObject.put("voorbeeld", jsonArray);
          
         //writing the JSONObject into a file 
         
         try{
             FileWriter fileWriter = new FileWriter("test.json");
             fileWriter.write(jsonObject.toJSONString());
             fileWriter.flush();
             fileWriter.close();
         }catch(Exception e){
             e.printStackTrace();
         }
         System.out.println(jsonObject);
    
    }
  
 
    
    
}
