readme wasdapp

1. SQL Workbench installeren 
2. Netbeans installeren
3. CI
4. Project laten runnen in Netbeans
------------------------------------------------------------------------
STAP 1. SQL Workbench


1. Installeer MySQL Workbench
2. Klik op de startpagina op de + om een nieuwe connectie te kunnen maken
3. Vul een connection name en wachtwoord in om een nieuwe connectie op te starten.
4. Voer dan het bijgevoegde create script uit om de database aan te maken.
-------------------------------------------------------------------------
STAP 2 : Netbeans

1. Installeer Netbeans
2. Klik bovenaan op het tabblad "File" en selecteer "Open project". 
3. Selecteer het project.
4. Om het project te laten runnen, druk F6 of klik op de run-knop(groen driehoekje).
-------------------------------------------------------------------------
STAP 3 : CI

Creër een folder in u c: schijf genaamd gitlab-runner.
Download de binary amd64.
-> Je vindt dit bestand op de de volgende url: https://docs.gitlab.com/runner/install/windows.html
Zet dit bestand in de aangemaakte folder en hernoem dit bestand naar gitlab-runner.exe

Vervolgens druk je op de windows key en zoek je voor cmd, dit voer je uit als administrator(rechtermuisklik en voer uit als administrator).
1. Voer het volgende commando uit:
gitlab-runner.exe register

2. Voer de gitlab url in:
https://gitlab.com 

3. Voer de token in die je gekregen hebt voor deze runner:
Twggu3Eg9yZ3Gz_L5hWN

4. Voer een omschrijving in voor deze runner:
my-runner    <- is een voorbeeld

5. Voer de tags in die geassocieerd zijn met de runner:
my-tag,another-tag      <- is een voorbeeld

6. Voer de runner executor uit:
shell


Vervolgens moeten we de runner installeren en starten. Dit doe je door de volgende commands.

1. Installeren van de runner:
gitlab-runner install

2. Starten van de runner:
gitlab-runner start
------------------------------------------------------------------------
STAP 4 : Project laten runnen 

1. Maak in de map input een nieuw bestand aan(met bestandsextensie .csv of .json). Als dit niet het 
   geval is, krijgt u een gepaste melding en de data wordt doorgestuurd. 
2. Als er iets gewijzigd is van naamgeving aan de bestanden, wordt er een melding weergegeven omdat 
   er gebruik wordt gemaakt van de watcher. 